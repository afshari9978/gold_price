from datetime import timedelta
from typing import List

import requests

from django.shortcuts import render
from khayyam import JalaliDate


# Create your views here.

def chart(request, money):
    end: JalaliDate = JalaliDate.today()
    start: JalaliDate = end - timedelta(days=4)

    start: str = f'{start.year}-{start.month}-{start.day}'

    response: List[dict] = requests.get(
        url=f'http://api.navasan.tech/ohlcSearch/?item=18ayar&start={start}&end={end.year}-{end.month}-{end.day}'
            f'&api_key=ijepvrhhlAHpSY0UYS0kwJdwDOPwJho6'
    ).json()
    context = {
        'days': [item.day for item in [end - timedelta(days=i) for i in range(5)]],
        'data': [int(day['close']) for day in response]
    }

    context['days'].reverse()
    context['total'] = int((money / context['data'][0]) * context['data'][-1] - money)

    return render(request, 'chart.html', context=context)
